# This is a maven plugin example

## how to compile

```
mvn clean install
```

## how to run

Two ways:

1. Create a maven project
2. run : `mvn hk.quantr:quantr-compiler-plugin:1.0:compileAssembly`

**Tips:** version is not required to run a standalone goal.

OR

1. Create a maven project
2. Add these to your pom.xml

```
<build>
	<plugins>
		<plugin>
			<groupId>hk.quantr</groupId>
			<artifactId>quantr-compiler-plugin</artifactId>
			<version>1.0</version>
			<executions>
				<execution>
					<phase>compile</phase>
					<configuration>
						<para1>para1 value</para1>
					</configuration>
					<goals>
						<goal>compileAssembly</goal>
					</goals>
				</execution>
			</executions>
		</plugin>
	</plugins>
</build>
```
3. execute `mvn compile`

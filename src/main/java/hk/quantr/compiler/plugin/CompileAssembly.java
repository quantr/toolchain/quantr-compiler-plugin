package hk.quantr.compiler.plugin;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import hk.quantr.javalib.CommonLib;

import java.nio.file.Path;
import javax.xml.transform.sax.SAXSource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "compileAssembly")
public class CompileAssembly extends AbstractMojo {

//    @Parameter(defaultValue = "https://quantr.foundation")
	private String mainFile;

	@Override
	public void execute() throws MojoExecutionException {
		File target = new File("target");
		if (!target.exists()) {
			target.mkdir();
		}
		File files[] = new File("src/main/riscv").listFiles();
		if (mainFile == null) {
			files = new File("src/main/riscv").listFiles();
		} else {
			files = new File[]{new File(mainFile)};
		}
		for (File file : files) {
			compile(file, target);
		}
	}

	private void compile(File file, File target) {
		try {
			System.out.println("Compiling " + file.getAbsolutePath());
			InputStream GetAssembler = CompileAssembly.class.getClassLoader().getResourceAsStream("assembler-1.5.jar");
			if (GetAssembler == null) {
				System.err.println("assembler.jar not exist");
				System.exit(1);
			}
			Path jarPath = Files.createTempFile("assembler", ".jar");
			FileUtils.copyInputStreamToFile(GetAssembler, jarPath.toFile());
			String command;
			String osName = System.getProperty("os.name").toLowerCase();

			String fileNameWithOutExt = FilenameUtils.removeExtension(file.getName());
			String lstPath = target.getAbsolutePath() + File.separator + fileNameWithOutExt + ".lst";
			String binPath = target.getAbsolutePath() + File.separator + fileNameWithOutExt + ".bin";

			if (osName.contains("windows")) {
				command = "\"" + System.getProperty("java.home") + "/bin/java\" -jar " + jarPath + " -a rv64 -l " + lstPath + " -o " + binPath + " " + file.getAbsolutePath();
			} else {
				command = System.getProperty("java.home") + "/bin/java -jar " + jarPath + " -a rv64 -l " + lstPath + " -o " + binPath + " " + file.getAbsolutePath();
			}

			System.out.println(CommonLib.runCommand(command));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

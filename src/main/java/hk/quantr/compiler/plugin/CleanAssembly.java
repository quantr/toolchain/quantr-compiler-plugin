package hk.quantr.compiler.plugin;

import hk.quantr.javalib.CommonLib;
import java.io.File;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name = "cleanAssembly")
public class CleanAssembly extends AbstractMojo {

	@Override
	public void execute() throws MojoExecutionException {
		File target = new File("target");
		CommonLib.deleteDirectory(target);
//		CompileAssembly buildAction = new CompileAssembly();
//		buildAction.execute();
	}
}
